package com.epam.gomel.tat;

public class HelloMain {
    public static void main(String[] args) {
        String myName = "Paul Golub";
        System.out.format("Hello from %s", myName);
    }
}
